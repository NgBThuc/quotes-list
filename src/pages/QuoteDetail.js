import { Fragment, useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Comments from "../components/Comments/Comments";
import QuoteCard from "../components/Quotes/QuoteCard";
import Button from "../components/UI/Button";
import Error from "../components/UI/Error";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import { fetchComments } from "../store/comments-actions";
import { fetchSingleQuote } from "../store/quotes-actions";

const QuoteDetail = () => {
  const dispatch = useDispatch();
  const [isShowComment, setIsShowComment] = useState(false);
  const params = useParams();
  const { quoteId } = params;

  const quoteData = useSelector((state) => state.quotes.currentQuote);
  const currentQuoteStatus = useSelector(
    (state) => state.ui.currentQuoteStatus
  );

  const errorHandler = () => {
    dispatch(fetchSingleQuote(quoteId));
  };

  useEffect(() => {
    dispatch(fetchSingleQuote(quoteId));
  }, [dispatch, quoteId]);

  const showCommentHandler = () => {
    setIsShowComment((prevState) => !prevState);
    dispatch(fetchComments(params.quoteId));
  };

  let quoteDetailContent;

  if (currentQuoteStatus.status === "PENDING") {
    quoteDetailContent = <LoadingSpinner />;
  }

  if (currentQuoteStatus.status === "ERROR") {
    quoteDetailContent = (
      <Error message={currentQuoteStatus.message} errorHandler={errorHandler} />
    );
  }

  if (currentQuoteStatus.status === "COMPLETE") {
    quoteDetailContent = (
      <Fragment>
        <QuoteCard text={quoteData.text} author={quoteData.author} />
        <div className="flex flex-col gap-5 items-center my-8">
          {isShowComment && <Comments />}
          {!isShowComment && (
            <Button
              onClick={showCommentHandler}
              className="hover:bg-teal-500 hover:text-white"
            >
              Load Comment
            </Button>
          )}
        </div>
      </Fragment>
    );
  }

  return <div className="w-3/5 mx-auto my-8">{quoteDetailContent}</div>;
};

export default QuoteDetail;
