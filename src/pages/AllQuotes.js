import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import QuotesList from "../components/Quotes/QuotesList";
import Button from "../components/UI/Button";
import Container from "../components/UI/Container";
import { commentsAction } from "../store/comments-slice";
import { fetchQuotesListData } from "../store/quotes-actions";

const sortQuotes = (quotes, ascending) => {
  return [...quotes].sort((quoteA, quoteB) => {
    if (ascending) {
      return quoteA.id > quoteB.id ? 1 : -1;
    } else {
      return quoteA.id < quoteB.id ? 1 : -1;
    }
  });
};

const AllQuotes = () => {
  const dispatch = useDispatch();
  const quotesListData = useSelector((state) => state.quotes.quotesList);
  const navigate = useNavigate();
  const location = useLocation();

  const queryParams = new URLSearchParams(location.search);
  const isSortingAscending = queryParams.get("sort") === "asc";

  const changeSortingHandler = () => {
    if (isSortingAscending) {
      navigate("?sort=desc");
    } else {
      navigate("?sort=asc");
    }
  };

  const sortedQuotesList = sortQuotes(quotesListData, isSortingAscending);

  useEffect(() => {
    dispatch(fetchQuotesListData());
    dispatch(commentsAction.setCommentsList([]));
  }, [dispatch, isSortingAscending]);

  return (
    <section>
      <Container className="my-10">
        <div className="w-3/5 mx-auto py-4 border-b-2 border-teal-500">
          <Button
            onClick={changeSortingHandler}
            className="text-teal-700 hover:text-white hover:bg-teal-500"
          >
            Sort {isSortingAscending ? "Descending" : "Ascending"}
          </Button>
        </div>
        <QuotesList quotesList={sortedQuotesList} />
      </Container>
    </section>
  );
};

export default AllQuotes;
