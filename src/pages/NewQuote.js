import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import NewQuoteInput from "../components/Quotes/NewQuoteInput";
import Container from "../components/UI/Container";
import Error from "../components/UI/Error";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import { uiActions } from "../store/ui-slice";

const NewQuote = () => {
  const newQuoteStatus = useSelector((state) => state.ui.newQuoteStatus);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const errorHandler = () => {
    dispatch(uiActions.setNewQuoteStatus(""));
  };

  let newQuoteContent = <NewQuoteInput />;

  if (newQuoteStatus.status === "PENDING") {
    newQuoteContent = <LoadingSpinner />;
  }

  if (newQuoteStatus.status === "ERROR") {
    newQuoteContent = (
      <Error message={newQuoteStatus.message} errorHandler={errorHandler} />
    );
  }

  useEffect(() => {
    if (newQuoteStatus.status === "COMPLETE") {
      navigate("/quotes");
      dispatch(uiActions.setNewQuoteStatus({ status: "", message: "" }));
    }
  }, [newQuoteStatus, navigate, dispatch]);

  return <Container className="my-10">{newQuoteContent}</Container>;
};

export default NewQuote;
