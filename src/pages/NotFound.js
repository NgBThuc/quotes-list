import { Link } from "react-router-dom";
import Button from "../components/UI/Button";
import Container from "../components/UI/Container";

const NotFound = () => {
  return (
    <Container className="my-10">
      <div className="text-center flex flex-col gap-8 items-center">
        <h1 className="text-4xl font-bold text-teal-700">Page Not Found!</h1>
        <Button className="hover:bg-teal-500 hover:text-white">
          <Link to="/">Return to Homepage</Link>
        </Button>
      </div>
    </Container>
  );
};

export default NotFound;
