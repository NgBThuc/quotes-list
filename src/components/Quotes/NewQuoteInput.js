import { useRef } from "react";
import { useDispatch } from "react-redux";
import { saveNewQuote } from "../../store/quotes-actions";
import Button from "../UI/Button";

const NewQuoteInput = () => {
  const authorInputRef = useRef();
  const textInputRef = useRef();
  const dispatch = useDispatch();

  const newQuoteSubmitHandler = (event) => {
    event.preventDefault();

    let newQuoteData = {
      author: authorInputRef.current.value,
      text: textInputRef.current.value,
    };

    dispatch(saveNewQuote(newQuoteData));
  };

  return (
    <form
      onSubmit={newQuoteSubmitHandler}
      className="w-3/5 mx-auto py-4 px-6 shadow-2xl border-2 border-teal-500 rounded-lg my-8 flex flex-col gap-4"
    >
      <div className="flex flex-col gap-4">
        <label htmlFor="quoteAuthor">Author</label>
        <input
          className="w-full py-2 px-4 rounded-lg border-2 border-transparent bg-teal-200 transition-colors duration-300 focus:outline-none focus:border-teal-700 hover:border-teal-700"
          type="text"
          id="quoteAuthor"
          name="author"
          ref={authorInputRef}
        />
      </div>
      <div className="flex flex-col gap-4">
        <label htmlFor="quoteText">Text</label>
        <textarea
          className="py-2 px-4 rounded-lg border-2 border-transparent bg-teal-200 transition-colors duration-300 focus:outline-none focus:border-teal-700 hover:border-teal-700 resize-none"
          name="text"
          id="quoteText"
          cols="30"
          rows="10"
          ref={textInputRef}
        ></textarea>
      </div>
      <div>
        <Button type="submit" className="hover:bg-teal-500 hover:text-white">
          Add Quote
        </Button>
      </div>
    </form>
  );
};

export default NewQuoteInput;
