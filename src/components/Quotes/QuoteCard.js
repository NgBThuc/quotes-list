const QuoteCard = (props) => {
  return (
    <div className="flex flex-col gap-4 py-20 px-10 rounded-lg bg-teal-900 text-white">
      <p className="text-3xl font-bold leading-relaxed">{props.text}</p>
      <p className="text-2xl italic text-right">{props.author}</p>
    </div>
  );
};

export default QuoteCard;
