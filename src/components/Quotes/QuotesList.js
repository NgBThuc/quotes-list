import { useDispatch, useSelector } from "react-redux";
import { fetchQuotesListData } from "../../store/quotes-actions";
import Error from "../UI/Error";
import LoadingSpinner from "../UI/LoadingSpinner";
import QuoteItem from "./QuoteItem";

const QuotesList = (props) => {
  const loadingStatus = useSelector((state) => state.ui.quotesLoadingStatus);
  const dispatch = useDispatch();

  const errorHandler = () => {
    dispatch(fetchQuotesListData());
  };

  const quotesList = props.quotesList.map((quote) => {
    return (
      <QuoteItem
        text={quote.text}
        author={quote.author}
        id={quote.id}
        key={quote.id}
      />
    );
  });

  let quotesListContent;

  if (loadingStatus.status === "PENDING") {
    quotesListContent = <LoadingSpinner />;
  }

  if (loadingStatus.status === "ERROR") {
    quotesListContent = (
      <Error message={loadingStatus.message} errorHandler={errorHandler} />
    );
  }

  if (loadingStatus.status === "COMPLETE") {
    quotesListContent = quotesList;
  }

  return (
    <div className="w-3/5 mx-auto my-8 flex flex-col gap-4">
      {quotesListContent}
    </div>
  );
};

export default QuotesList;
