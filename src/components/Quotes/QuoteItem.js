import { Link } from "react-router-dom";
import Button from "../UI/Button";

const QuoteItem = (props) => {
  return (
    <div className="flex items-center justify-between gap-4 bg-teal-400 p-5 rounded-lg shadow-lg">
      <div className="flex flex-col gap-3">
        <p className="text-lg font-bold">{props.text}</p>
        <p className="text-sm font-bold italic text-teal-800">{props.author}</p>
      </div>
      <div>
        <Button className="bg-teal-500 text-white hover:bg-teal-300 hover:border-2 hover:border-teal-800 hover:text-teal-800">
          <Link to={`/quotes/${props.id}`}>View Full Screen</Link>
        </Button>
      </div>
    </div>
  );
};

export default QuoteItem;
