const CommentItem = (props) => {
  return (
    <div className="w-4/5 mx-auto bg-teal-200 rounded-lg text-center py-4">
      <p>{props.content}</p>
    </div>
  );
};

export default CommentItem;
