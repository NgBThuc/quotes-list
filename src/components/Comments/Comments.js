import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchComments } from "../../store/comments-actions";
import Error from "../UI/Error";
import LoadingSpinner from "../UI/LoadingSpinner";
import CommentsList from "./CommentsList";
import NewComment from "./NewComment";

const Comments = () => {
  const commentsStatus = useSelector((state) => state.ui.commentsStatus);
  const dispatch = useDispatch();
  const params = useParams();

  const errorHandler = () => {
    dispatch(fetchComments(params.quoteId));
  };

  let commentContent = <CommentsList />;

  if (commentsStatus.status === "PENDING") {
    commentContent = <LoadingSpinner />;
  }

  if (commentsStatus.status === "ERROR") {
    commentContent = (
      <Error message={commentsStatus.message} errorHandler={errorHandler} />
    );
  }

  return (
    <Fragment>
      <h2 className="text-3xl font-semibold text-teal-700">User Comment</h2>
      <NewComment />
      {commentContent}
    </Fragment>
  );
};

export default Comments;
