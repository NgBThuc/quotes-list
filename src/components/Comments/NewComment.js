import { useRef } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { addComment } from "../../store/comments-actions";
import Button from "../UI/Button";

const NewComment = (props) => {
  const commentInputRef = useRef();
  const params = useParams();
  const dispatch = useDispatch();

  const newCommentSubmitHandler = (event) => {
    event.preventDefault();
    const commentData = {
      quoteId: params.quoteId,
      content: commentInputRef.current.value,
    };
    dispatch(addComment(commentData));
    commentInputRef.current.value = "";
  };

  return (
    <form
      onSubmit={newCommentSubmitHandler}
      className="flex flex-col gap-2 w-full text-center items-center"
    >
      <label htmlFor="quoteComment" className="text-xl font-semibold">
        Your Comment
      </label>
      <input
        className="w-4/5 py-2 px-4 rounded-lg border-2 border-transparent bg-teal-200 transition-colors duration-300 focus:outline-none focus:border-teal-700 hover:border-teal-700"
        id="quoteComment"
        type="text"
        ref={commentInputRef}
      />
      <Button type="submit" className="hover:bg-teal-500 hover:text-white">
        Add Comment
      </Button>
    </form>
  );
};

export default NewComment;
