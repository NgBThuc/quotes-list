import { useSelector } from "react-redux";
import CommentItem from "./CommentItem";

const CommentsList = () => {
  const commentsList = useSelector((state) => state.comments.commentsList);

  return (
    <div className="w-full flex flex-col gap-3">
      {commentsList.map((comment) => (
        <CommentItem key={comment.id} content={comment.content} />
      ))}
    </div>
  );
};

export default CommentsList;
