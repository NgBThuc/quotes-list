import { NavLink } from "react-router-dom";
import Container from "../UI/Container";

const Header = () => {
  const activeClasses = "underline underline-offset-8 text-white";

  return (
    <header className="bg-teal-600 text-white py-6">
      <Container className="flex justify-between items-center">
        <NavLink to="/" className="text-3xl">
          Great Quotes
        </NavLink>
        <nav>
          <ul className="flex gap-10 text-teal-200">
            <li className="transition-color duration-300 cursor-pointer hover:text-white">
              <NavLink
                className={(navData) => (navData.isActive ? activeClasses : "")}
                to="/quotes"
              >
                All Quotes
              </NavLink>
            </li>
            <li className="transition-color duration-300 cursor-pointer hover:text-white">
              <NavLink
                className={(navData) => (navData.isActive ? activeClasses : "")}
                to="/new-quote"
              >
                Add A Quotes
              </NavLink>
            </li>
          </ul>
        </nav>
      </Container>
    </header>
  );
};

export default Header;
