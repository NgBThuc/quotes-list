import Button from "./Button";

const Error = (props) => {
  console.log("Hello from Error");
  return (
    <div className="flex flex-col gap-4 items-center">
      <div className="text-center text-3xl font-bold text-teal-700">
        {props.message}
      </div>
      <Button
        onClick={props.errorHandler}
        className="hover:bg-teal-500 hover:text-white"
      >
        Try Again
      </Button>
    </div>
  );
};

export default Error;
