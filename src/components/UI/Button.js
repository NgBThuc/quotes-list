const Button = (props) => {
  const buttonClasses = `whitespace-nowrap py-2 px-4 rounded-lg border-2 border-teal-500 transition-colors duration-300 ${props.className}`;
  return (
    <button type={props.type} onClick={props.onClick} className={buttonClasses}>
      {props.children}
    </button>
  );
};

export default Button;
