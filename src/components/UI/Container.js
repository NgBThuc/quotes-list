const Container = (props) => {
  const containerClasses = `container mx-auto ${props.className}`;

  return <div className={containerClasses}>{props.children}</div>;
};

export default Container;
