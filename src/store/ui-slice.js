import { createSlice } from "@reduxjs/toolkit";

const uiInitialState = {
  quotesLoadingStatus: {
    status: "",
    message: "",
  },
  newQuoteStatus: {
    status: "",
    message: "",
  },
  currentQuoteStatus: {
    status: "",
    message: "",
  },
  commentsStatus: {
    status: "",
    message: "",
  },
};

const uiSlice = createSlice({
  name: "ui",
  initialState: uiInitialState,
  reducers: {
    setQuotesLoadingStatus(state, action) {
      const { status, message } = action.payload;
      state.quotesLoadingStatus.status = status;
      state.quotesLoadingStatus.message = message;
    },
    setNewQuoteStatus(state, action) {
      const { status, message } = action.payload;
      state.newQuoteStatus.status = status;
      state.newQuoteStatus.message = message;
    },
    setCurrentQuoteStatus(state, action) {
      const { status, message } = action.payload;
      state.currentQuoteStatus.status = status;
      state.currentQuoteStatus.message = message;
    },
    setCommentsStatus(state, action) {
      const { status, message } = action.payload;
      state.commentsStatus.status = status;
      state.commentsStatus.message = message;
    },
  },
});

export const uiActions = uiSlice.actions;
export default uiSlice;
