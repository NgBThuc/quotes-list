import { BASE_URL } from "../constants/constants";
import { commentsAction } from "./comments-slice";
import { uiActions } from "./ui-slice";

export const addComment = (commentData) => {
  return async (dispatch) => {
    const uploadDataToAPI = async () => {
      await fetch(`${BASE_URL}/comments/${commentData.quoteId}.json`, {
        method: "POST",
        body: JSON.stringify({
          content: commentData.content,
        }),
      });
    };

    try {
      dispatch(uiActions.setCommentsStatus({ status: "PENDING" }));
      await uploadDataToAPI();
      dispatch(fetchComments(commentData.quoteId));
      dispatch(uiActions.setCommentsStatus({ status: "COMPLETE" }));
    } catch (error) {
      dispatch(
        uiActions.setCommentsStatus({
          status: "ERROR",
          message: "Add New Comment Failed!",
        })
      );
    }
  };
};

export const fetchComments = (quoteId) => {
  return async (dispatch) => {
    const takeDataFromAPI = async (quoteId) => {
      const response = await fetch(`${BASE_URL}/comments/${quoteId}.json`);
      const data = await response.json();

      if (!data) {
        return [];
      }

      let commentsData = [];

      for (let key in data) {
        let commentItemData = {
          id: key,
          content: data[key].content,
        };
        commentsData.push(commentItemData);
      }

      return commentsData;
    };

    try {
      dispatch(uiActions.setCommentsStatus({ status: "PENDING" }));
      let commentsData = await takeDataFromAPI(quoteId);
      dispatch(commentsAction.setCommentsList(commentsData));
      dispatch(uiActions.setCommentsStatus({ status: "COMPLETE" }));
    } catch (error) {
      dispatch(
        uiActions.setCommentsStatus({
          status: "ERROR",
          message: "Load Comment Data Failed!",
        })
      );
    }
  };
};
