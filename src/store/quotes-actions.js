import { BASE_URL } from "../constants/constants";
import { quotesAction } from "./quotes-slice";
import { uiActions } from "./ui-slice";

export const fetchQuotesListData = () => {
  return async (dispatch) => {
    const takeDataFromAPI = async () => {
      const respone = await fetch(`${BASE_URL}/quotes.json`);

      const data = await respone.json();

      let quotesListData = [];

      for (const key in data) {
        let quoteItemData = {
          id: key,
          author: data[key].author,
          text: data[key].text,
        };
        quotesListData.push(quoteItemData);
      }

      return quotesListData;
    };

    try {
      dispatch(uiActions.setQuotesLoadingStatus({ status: "PENDING" }));
      const quotesListData = await takeDataFromAPI();
      dispatch(quotesAction.setQuotesListData(quotesListData));
      dispatch(uiActions.setQuotesLoadingStatus({ status: "COMPLETE" }));
    } catch (error) {
      dispatch(
        uiActions.setQuotesLoadingStatus({
          status: "ERROR",
          message: "Load Quotes Data Failed",
        })
      );
    }
  };
};

export const saveNewQuote = (newQuoteData) => {
  return async (dispatch) => {
    const uploadDataToAPI = async (newQuoteData) => {
      await fetch(`${BASE_URL}/quotes.json`, {
        method: "POST",
        body: JSON.stringify(newQuoteData),
      });
    };

    try {
      dispatch(uiActions.setNewQuoteStatus({ status: "PENDING" }));
      await uploadDataToAPI(newQuoteData);
      dispatch(uiActions.setNewQuoteStatus({ status: "COMPLETE" }));
      dispatch(fetchQuotesListData());
    } catch (error) {
      dispatch(
        uiActions.setNewQuoteStatus({
          status: "ERROR",
          message: "Add New Quote Failed!",
        })
      );
    }
  };
};

export const fetchSingleQuote = (quoteId) => {
  return async (dispatch) => {
    const takeDataFromAPI = async () => {
      const response = await fetch(`${BASE_URL}/quotes/${quoteId}.json`);
      const data = await response.json();
      return data;
    };

    try {
      dispatch(uiActions.setCurrentQuoteStatus({ status: "PENDING" }));
      const data = await takeDataFromAPI();
      dispatch(quotesAction.setCurrentQuoteData(data));
      dispatch(uiActions.setCurrentQuoteStatus({ status: "COMPLETE" }));
    } catch (error) {
      dispatch(
        uiActions.setCurrentQuoteStatus({
          status: "ERROR",
          message: "Load Quote Data Failed",
        })
      );
    }
  };
};
