import { createSlice } from "@reduxjs/toolkit";

const commentsInitialState = {
  commentsList: [],
};

const commentsSlice = createSlice({
  name: "comment",
  initialState: commentsInitialState,
  reducers: {
    setCommentsList(state, action) {
      state.commentsList = action.payload;
    },
  },
});

export const commentsAction = commentsSlice.actions;
export default commentsSlice;
