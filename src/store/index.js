import { configureStore } from "@reduxjs/toolkit";
import commentsSlice from "./comments-slice";
import quotesSlice from "./quotes-slice";
import uiSlice from "./ui-slice";

const store = configureStore({
  reducer: {
    quotes: quotesSlice.reducer,
    ui: uiSlice.reducer,
    comments: commentsSlice.reducer,
  },
});

export default store;
