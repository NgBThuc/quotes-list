import { createSlice } from "@reduxjs/toolkit";

const quotesInitialState = {
  quotesList: [],
  currentQuote: {},
};

const quotesSlice = createSlice({
  name: "quotes",
  initialState: quotesInitialState,
  reducers: {
    setQuotesListData(state, action) {
      const quotesListData = action.payload;
      state.quotesList = quotesListData;
    },
    setCurrentQuoteData(state, action) {
      const currentQuoteData = action.payload;
      state.currentQuote = currentQuoteData;
    },
  },
});

export const quotesAction = quotesSlice.actions;
export default quotesSlice;
